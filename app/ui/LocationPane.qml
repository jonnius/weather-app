/*
 * Copyright (C) 2015, 2017 Canonical Ltd
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import "../components"
import "../data/suncalc.js" as SunCalc


ListView {
    id: mainPageWeekdayListView
    height: parent.height
    model: ListModel {

    }
    width: weatherApp.width

    /*
      Data properties
    */
    property string name
    property string currentTemp
    property string icon
    property string iconName

    property var hourlyForecastsData
    property string hourlyTempUnits

    property var todayData
    property bool graphicVisible : false

    property var lastFetch  // don't store as int as reaches max int

    delegate: DayDelegate {
        day: model.day
        high: model.high
        image: model.image
        low: model.low

        modelData: model
    }
    header: ListItem {
        divider {
            visible: true
        }
        height: locationTop.height

        Column {
            id: locationTop

            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }
            spacing: units.gu(1)

            Row {  // spacing at top
                height: units.gu(1)
                width: parent.width
            }

            HeaderRow {
                id: headerRow
                locationName: mainPageWeekdayListView.name
            }

            HomeGraphic {
                id: homeGraphic
                icon: mainPageWeekdayListView.icon
                visible: graphicVisible;

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        graphicVisible = false;
                    }
                }
            }

            Loader {
                id: homeHourlyLoader
                active: !homeGraphic.visible
                asynchronous: true
                height: units.gu(32)
                source: "../components/HomeHourly.qml"
                visible: active
                width: parent.width
            }

            HomeTempInfo {
                id: homeTempInfo
                modelData: todayData
                now: mainPageWeekdayListView.currentTemp
                updatedAt: mainPageWeekdayListView.lastFetch
            }

            NumberAnimation {
                id: scrollToTopAnimation
                target: mainPageWeekdayListView;
                property: "contentY";
                duration: 200;
                easing.type: Easing.InOutQuad
                to: -height
            }

            Connections {
                target: locationPages
                onCurrentIndexChanged: {
                    if (locationPages.currentIndex !== index) {
                        scrollToTopAnimation.start()
                    } else {
                        mainPageWeekdayListView.contentY = -locationTop.height
                    }
                }
            }
        }
    }

    PullToRefresh {
        id: pullToRefresh
        refreshing: false

        onRefresh: {
            locationPages.loaded = false
            refreshing = true
            refreshData(false, true)
        }
    }

    //use simpler version for first commit due to small inaccuracies
    //moonphase returned as value between 0 and 1,
    //approx. 29.5 days per cycle, equals to 0.034 per day
    function getMoonPhaseString(phase) {
        var moonPhaseString = "";
        if (phase >= 0.00 && phase < 0.025) {
           moonPhaseString = i18n.tr("New moon");
        } else if (phase > 0.025 && phase < 0.475) {
           moonPhaseString = i18n.tr("Waxing moon");
        } else if (phase > 0.475 && phase < 0.525) {
           moonPhaseString = i18n.tr("Full moon");
        } else if (phase > 0.525 && phase < 0.975) {
           moonPhaseString = i18n.tr("Waning moon");
        } else if (phase > 0.975 ) {
           moonPhaseString = i18n.tr("New moon");
        }
        return moonPhaseString;
    }
    // //moonphase returned as value between 0 and 1,
    // //approx. 29 days per cycle, equals to 0.035 per day
    // function getMoonPhaseString(phase) {
    //     var moonPhaseString = ""
    //     if (phase == 1) {
    //        phase = 0;
    //     } else if (phase >= 0.00 && phase < 0.02) {
    //        moonPhaseString = 'New Moon';
    //     } else if (phase > 0.02 && phase < 0.23) {
    //        moonPhaseString = 'Waxing Crescent';
    //     } else if (phase > 0.23 && phase < 0.27) {
    //        moonPhaseString = 'First Quarter';
    //     } else if (phase > 0.27 && phase < 0.48) {
    //        moonPhaseString = 'Waxing Gibbous';
    //     } else if (phase > 0.48 && phase < 0.52) {
    //        moonPhaseString = 'Full Moon';
    //     } else if (phase > 0.52 && phase < 0.73) {
    //        moonPhaseString = 'Waning Gibbous';
    //     } else if (phase > 0.73 && phase < 0.77) {
    //        moonPhaseString = 'LastQuarter';
    //     } else if (phase > 0.77 && phase < 0.98) {
    //        moonPhaseString = 'Waning Crescent';
    //     } else if (phase > 0.98 ) {
    //        moonPhaseString = 'New Moon';
    //     }
    //     return moonPhaseString;
    // }

    function getDayData(data) {
        var tempUnits = settings.tempScale === "°C" ? "metric" : "imperial"
        var windUnits = settings.windUnits === "kph" ? "metric" : "imperial";
        var timezoneOffset = new Date().getTimezoneOffset();  //timezone offset in minutes
        var offset = (data.location.timezone && data.location.timezone.dstOffset !== undefined) ? (data.location.timezone.dstOffset*60 + timezoneOffset)*60*1000: 0
        var options = { timeZone: data.location.timezone.timeZoneId, timeZoneName: 'long' };
        var sunrise = new Date(SunCalc.SunCalc.getTimes(getDate(data.date), data.location.coord.lat, data.location.coord.lon).sunrise.getTime() + offset/60);
        var sunset = new Date(SunCalc.SunCalc.getTimes(getDate(data.date), data.location.coord.lat, data.location.coord.lon).sunset.getTime() + offset/60);
        var moonphase = SunCalc.SunCalc.getMoonIllumination(getDate(data.date)).phase.toString();

        // TRANSLATORS: "%1kph" and "%1mph" is to be able to add
        // an optional space between digit+unit i.e. In English '9kph', in Catalan '9 km/h'
        var windSpeedString = settings.windUnits === "kph" ? i18n.tr("%1kph") : i18n.tr("%1mph");

        console.log(windSpeedString)

        return {
            day: formatTimestamp(data.date,"ddd") + " " + formatTimestampLocaleShort(data.date),
            low: Math.round(data[tempUnits].tempMin).toString() + settings.tempScale,
            high: (data[tempUnits].tempMax !== undefined) ? Math.round(data[tempUnits].tempMax).toString() + settings.tempScale : "–",
            image: (data.icon !== undefined && iconMap[data.icon] !== undefined) ? iconMap[data.icon] : "",
            condition: emptyIfUndefined(data.condition),
            chanceOfPrecip: emptyIfUndefined(data.propPrecip, "%"), //not supported by OpenWeatherMap json API
            humidity: emptyIfUndefined(data.humidity, "%"),
            sunrise: data.sunrise || sunrise.toLocaleTimeString(Qt.locale().name, options),
            sunset: data.sunset || sunset.toLocaleTimeString(Qt.locale().name, options),
            moonPhase: getMoonPhaseString(moonphase), //  + " " + Math.round(moonphase*1000)/1000, //to display value for tests
            uvIndex: emptyIfUndefined(data.uv),
            wind: data[windUnits].windSpeed === undefined || data.windDir === undefined
                        ? "" : windSpeedString.arg(Math.round(data[windUnits].windSpeed)) + " " + data.windDir
        };
    }

    function emptyIfUndefined(variable, append) {
        if (append === undefined) {
            append = ""
        }

        return variable === undefined ? "" : variable + append
    }

    /*
      Extracts values from the location weather data and puts them into the appropriate components
      to display them.

      Attention: Data access happens through "weatherApp.locationList[]" by index, since complex
      data in models will lead to type problems.
    */
    function renderData(index) {
        var data = weatherApp.locationsList[index],
                current = data.data[0].current,
                forecasts = data.data,
                forecastsLength = forecasts.length,
                hourlyForecasts = [];

        var tempUnits = settings.tempScale === "°C" ? "metric" : "imperial"

        // set general location data
        name = data.location.name;

        // set current temps and condition
        iconName = (current.icon) ? current.icon : "";
        icon = (imageMap[iconName] !== undefined) ? imageMap[iconName] : "";
        currentTemp = Math.round(current[tempUnits].temp).toString() + String("°");

        // reset days list
        // TODO: overwrite and trim to make the refresh smoother?
        mainPageWeekdayListView.model.clear()

        // set daily forecasts
        if(forecastsLength > 0) {
            for(var x=0;x<forecastsLength;x++) {
                // collect hourly forecasts if available
                if(forecasts[x].hourly !== undefined && forecasts[x].hourly.length > 0) {
                    hourlyForecasts = hourlyForecasts.concat(forecasts[x].hourly)
                }

                // Copy the coords and timezone of the location
                // so that sun{rise,set} work with OWM
                forecasts[x].location = {
                    coord: data.location.coord,
                    timezone: data.location.timezone
                };

                if (x === 0) {
                    // store today's data for later use
                    todayData = getDayData(forecasts[x]);
                } else {
                    // set daydata
                    mainPageWeekdayListView.model.append(getDayData(forecasts[x]));
                }
            }
        }

        // set data for hourly forecasts
        if(hourlyForecasts.length > 0) {
            hourlyForecastsData = hourlyForecasts;
            hourlyTempUnits = tempUnits;
        }

        // Set last updated time
        mainPageWeekdayListView.lastFetch = data.updated;
    }

    Component.onCompleted: renderData(index)
}
